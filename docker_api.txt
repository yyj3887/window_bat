===================DOCKER 볼륨 마운트===============================

[docker에 대해 최소한 알아야 할것]
         도커는 무상태를 지향한다. 그렇기 때문에 프로그램^(컨테이너^)을 실행시킨다음 생성된 데이터는 프로그램^(컨테이너^)을 종료하면
         데이터는 저장되지 않고 사라진다. 그렇기 때문에 oracle을 docker로 사용하려면 데이터를 프로그램^(컨테이너^)외부에 저장해야 한다.
        그래서 -v 옵션으로 외부에 데이터 저장할 것 이다.
 
[oracle 설치]
        도커로 사용하고 싶은 프로그램이 있다면 구글에 ‘docker 프로그램이름’을 검색하거나 도커허브에서 원하는 프로그램을 검색하면 된다.
        지금은 oracle 12c를 사용하겠다.
 
       docker run --name oracle12c -d -p 8080:8080 -p 1521:1521 sath89/oracle-12c ###실행금지.
       이렇게 하면 오라클이 실행 될것이다. 
       다만 위에서 언급한대로 데이터는 컨테이너를 중지하는 순간 사라질것이다. 
       혹시나 위 명령어를 실행했으면 이미 [이미지가 생성되서 동일한 이름이 있기 때문에 아래 명령어로 이미지를 지우거나 새로운 컨테이너 이름을 사용해야 한다.]

       docker run --name oracle12c -d -p 8080:8080 -p 1521:1521 -v ~/my/oracle/data:/u01/app/oracle sath89/oracle-12c
       이렇게 -v 옵션으로 저장될 위치를 설정해주면 해당 위치에 데이터를 저장하기 때문에 데이터는 보존된다.
        위 명령어는 ‘~/my/oracle/data’라는 경로에 데이터를 저장하겠다는 의미므로 다른 위치에 저장하고 싶으면 해당 문구를 변경하면 된다.

[docker에 볼륨생성]

 docker Volume create [볼륨명]
 docker Volume create myoracle_Data

[docker에 볼륨리스트확인]

 docker Volume ls

[docker에 볼륨정보확인]

 docker Volume inspect [볼륨명]
 docker inspect myoracle_Data

===================SSH MobaXterm 접속할때===========================
[user docker id , pw]
    id : docker
    pwd : t
===================Window에 리눅스vm머신을 사용할경우===========================

Window의 경우는 가상머신을 통하여 docker를 띄워야 실행가능.
하여 docker-machine ls 를 통하여 가상머신의 환경부터 확인한후 docker명령어를 실행한다.
docker-machine을 확인하여 status상태가 꺼짐상태이면 docker 실행불가능.

가상머신 컨트롤 중요 옵션 이외의 명령어 docker-machine -help 를통하여 확인가능.  
rm                    Remove a machine ex : docker-machine rm  
start                 Start a machine  ex : docker-machine start 
stop                  Stop a machine   ex : docker-machine stop 
===================DOKER error===========================

%docker_err01%                                 err : docker 먹통일때
%docker_err02%                                 err : docker images 삭제 안될때 using its referenced 
===================DOKER INSTALL=========================
https://github.com/docker/toolbox/releases       docker toolbox exe 파일 다운 설치
===================DOCKER 명령어==========================	

docker-machine.exe ls                            DOCKER VM 상태정보 확인 IP , PORT

docker search [이미지 이름]                       DOCKER 허브 목록 출력

docker pull [이미지 이름]:[태그]                  DOCEKR 허브에서 이미지 가져오기

docker images                                    DOCEKR 이미지리스트 출력

docker rmi [이미지 이름]                          DOCEKR 이미지삭제

docker ps                                        DOCKER 컨테이너 목록 출력

docker create [옵션] [이미지 이름]:[태그]         DOCKER 컨테이너 생성

docker start [이미지 이름]:[태그]                 DOCEKR 컨테이너 실행

docker container run --name -d -p [포트]:[포트]   DOCEKR 컨테이너 실행

docker container rm "컨테이너이름"                DOCEKR 컨테이너 삭제

docker container prune                           DOCEKR 중지된 컨테이너 삭제

docker stop                                      DOCEKR 컨테이너 중지

docker attach [이미지 이름]:[태그]                DOCKER 컨테이너 내부 쉘로 들어가기

docker run [옵션] [이미지 이름]:[태그]            DOCKER 생성 -^> 실행 -^> 쉘로들어가기 한번에 실행

docker exec -it mysql_test /bin/bash             DOCEKR 내부 터미널 실행

docker commit [이미지 이름] [이미지]             DOCEKR container 변경사항저장

docker info                                     DOCKER 상세정보 확인가능 , ##이미지 저장 , 스토리지 등등..

    ##docker info명령어를 실행했을때

            Server:
         Containers: 9                           
          Running: 0
          Paused: 0
          Stopped: 9
         Images: 6
         Server Version: 19.03.12
         Storage Driver: overlay2                             
          Backing Filesystem: extfs
          Supports d_type: true
          Native Overlay Diff: true
         Logging Driver: json-file
         Cgroup Driver: cgroupfs
         Plugins:
          Volume: local
          Network: bridge host ipvlan macvlan null overlay
          Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
         Swarm: inactive
         Runtimes: runc
         Default Runtime: runc
         Init Binary: docker-init
         containerd version: 7ad184331fa3e55e52b890ea95e65ba581ae3429
         runc version: dc9208a3303feef5b3839f4323d9beb36df0a9dd
         init version: fec3683
         Security Options:
          seccomp
           Profile: default
         Kernel Version: 4.19.130-boot2docker
         Operating System: Boot2Docker 19.03.12 (TCL 10.1)
         OSType: linux
         Architecture: x86_64
         CPUs: 1
         Total Memory: 985.4MiB
         Name: default
         ID: UBNA:M3IB:HIYP:O2JQ:6W3I:6SNI:MMFT:SQBQ:3E67:4BMV:YYLD:465R
         Docker Root Dir: /mnt/sda1/var/lib/docker                                  ##docker가 설치된 위치
         Debug Mode: false
         Registry: https://index.docker.io/v1/
         Labels:
          provider=virtualbox
         Experimental: false
         Insecure Registries:
          127.0.0.0/8
         Live Restore Enabled: false
         Product License: Community Engine

실제 이경로로 이동을하면 /mnt/sda1/var/lib/docker , overlay2 , images , container 폴더를 확인할수있고 , 여기에 저장이된다.
sudo du -sh [폴더명]을 치면 폴더의 용량을 확인할수있다.
===================DOCKER 옵션==========================    
[Options] $ docker run [Options] [Image] [Command]

     -it
             -i : interactive, -t ; tty
             이 옵션을 줘야 컨테이너 안에서 터미널 실행 가능

     -p  [호스트의 포트:컨테이너 포트]
      
             -p 8888:8888
     
             도커 호스트의 포트 : 컨테이너 내부 포트
             Tensorboard등의 실행에 염두해 포트를 여러개 열어둘 수도 있음
      
             -p 8888:8888 -p 6006-6015:6006-6015
      
     --name [컨테이너 이름]
     
             --name pt
     
     -v [Host Path : Container Path]
     
             -v ${HOME}/dev:/data
     
             보여질 호스트 폴더 : 컨테이너의 폴더

==========================================================
===================DOCKER 관련 홈페이지 ==================
DOCKER 이미지 저장소    https://hub.docker.com/
===================MY SQL 컨테이너=========================
docker run -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=password --name mysql_test mysql

===================CENTOS 변경내용 저장=============================
1.docker commit 22aca64d9d3a centos            ex^) 22aca64d9d3a^(container id^)   변경내용 저장
2.commit을수행하면 이미지로 만들어짐. docker images 로 확인 해당 커밋내용을 불러와서 실행

===================CENTOS 변경내용 실행=============================
1.docker images 이미지 내용확인
3.docker run -i -t -u 202034-365564 [이미지이름]:[태그] 실행 ex^)docker run -i -t -u 202034-365564 bangsong/centos
===========================================================


==================CENTOS 명령어 LINUX =====================
    
cat /etc/passwd 파일에 사용자 정보 담겨있음.

root:x:0:0:root:/root:/bin/bash
필드 1 : 사용자명
필드 2 : 패스워드^(/etc/shadow 파일에 암호화되어 있음^)
필드 3 : 사용자 계정 uid
필드 4 : 사용자 계정 gid
필드 5 : 사용자 계정 이름^(정보^)
필드 6 : 사용자 계정 홈 디렉토리
필드 7 : 사용자 계정 로그인 쉘

사용자 추가.
useradd "사용자 이름"

사용자변경
usermod [옵션] "사용자 이름"    

비밀번호 설정
passwd "사용자 이름"  

옵션

-c 사용자 이름 또는 정보 ex: usermod -c "양영재" "사용자 이름"
-d 홈디렉토리변경 ex: usermode -d /home/2020 "사용자이름"
-e 사용자 유효기간
=======================================================