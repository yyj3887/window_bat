echo off

SET LOCALSAPCE=D:\dev_workspace
SET BLOG_ROOT=D:\dev_workspace\docs
SET WINBAT_ROOT=D:\dev_workspace\window_bat

if exist %LOCALSAPCE% (
        if exist %BLOG_ROOT% (
			D:
			cd %BLOG_ROOT%
			git pull	
		) else (
            D:
		    CD %LOCALSAPCE%
	     	echo git clone..
		    git clone https://gitlab.com/yyj3887/docs.git
		)		

		if exist %WINBAT_ROOT% (
			D:
			cd %WINBAT_ROOT%
			git pull	
		) else (			
			D:
		    CD %LOCALSAPCE%
	     	echo git clone..
		    git clone https://gitlab.com/yyj3887/window_bat.git
		)		
        pause
	) else (
		echo mkdir..%LOCALSAPCE%
		mkdir %LOCALSAPCE%
		D:
		CD %LOCALSAPCE%
		echo git clone winbat..
		git clone https://gitlab.com/yyj3887/window_bat.git        

        echo git clone blog..
		git clone https://gitlab.com/yyj3887/docs.git        
        pause
	)